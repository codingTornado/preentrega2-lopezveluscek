import { useState } from "react";
import PropTypes from "prop-types";

import "./button.css";

function Button({ children, color, onClick: handleClick, type }) {
  const [colorState, setColorState] = useState({
    backgroundColor: color,
    borderColor: "red"
  });

  return (
    <button
      onClick={handleClick}
      style={colorState}
      className={`btn ${type || ""}`}
    >
      {children}
    </button>
  );
}

Button.propTypes = {
  children: PropTypes.node,
  color: PropTypes.string,
  onClick: PropTypes.func,
  type: PropTypes.string
};

export default Button;
