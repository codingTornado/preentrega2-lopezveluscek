import { useState } from "react";
import PropTypes from "prop-types";

import InputForm from "./InputForm";

function BuyForm({ onSubmit }) {
  const [userData, setUserData] = useState({
    name: "",
    email: "",
    phone: ""
  });

  console.log(userData);

  function onInputChange(evt) {
    const inputName = evt.target.name;
    const value = evt.target.value;

    const newUserData = { ...userData };
    newUserData[inputName] = value;
    setUserData(newUserData);
  }

  function handleSubmit(evt) {
    evt.preventDefault();
    onSubmit(userData);
  }

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <div>
          <InputForm
            required="true"
            title="Nombre"
            name="name"
            value={userData.name}
            onInputChange={onInputChange}
          />
        </div>
        <div>
          <InputForm
            required="true"
            title="Email"
            name="email"
            value={userData.email}
            onInputChange={onInputChange}
          />
        </div>
        <div>
          <InputForm
            required="true"
            title="Teléfono"
            name="phone"
            value={userData.phone}
            onInputChange={onInputChange}
          />
        </div>
      </div>

      <button onClick={handleSubmit}>Crear orden</button>
    </form>
  );
}

BuyForm.propTypes = {
  onSubmit: PropTypes.func
};

export default BuyForm;
