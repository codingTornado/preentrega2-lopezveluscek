import { useContext } from "react";
import { useNavigate } from "react-router-dom";

import Swal from "sweetalert2";

import { createBuyOrderFirestoreWithStock } from "services/firebase";

import cartContext from "storage/CartContext";

import BuyForm from "./BuyForm";
import Button from "components/Button/Button";
import FlexWrapper from "components/FlexWrapper/FlexWrapper";

function CartView() {
  const { cart, clear, removeItem, totalPriceInCart } = useContext(cartContext);
  const navigate = useNavigate();

  if (cart.length === 0) return <h1>Carrito Vacio</h1>;

  function createBuyOrder(userData) {
    const buyData = {
      buyer: userData,
      items: cart,
      total: totalPriceInCart(),
      date: new Date()
    };

    createBuyOrderFirestoreWithStock(buyData).then((orderId) => {
      clear();
      navigate(`/checkout/${orderId}`);
      Swal.fire({
        title: `Gracias por tu compra`,
        text: `El identificador de tu orden es ${orderId}`,
        icon: "success"
      });
    });
  }

  return (
    <div style={{ padding: "4em" }}>
      <div>
        <h1>Este es el contenido de tu carrito</h1>
        <FlexWrapper>
          {cart.map((cartItem) => (
            <div className="card" key={cartItem.id}>
              <div className="card-img">
                <img src={cartItem.thumbnail} alt={cartItem.title} />
              </div>
              <div className="card-detail">
                <h3>{cartItem.title}</h3>
                <h4>$ {cartItem.price}</h4>
                <h4>Cantidad: {cartItem.count}</h4>
                <h4>Precio a pagar: {cartItem.count * cartItem.price}</h4>
                <Button onClick={() => removeItem(cartItem.id)} type="danger">
                  X
                </Button>
              </div>
            </div>
          ))}
        </FlexWrapper>
      </div>
      <div>
        <Button type="danger" onClick={clear}>
          Vaciar Carrito
        </Button>
      </div>
      <h2>Total a pagar: ${totalPriceInCart()}</h2>

      <div className="buy-form">
        <h2>Crear orden de compra</h2>
        <BuyForm onSubmit={createBuyOrder} />
      </div>
    </div>
  );
}

export default CartView;
