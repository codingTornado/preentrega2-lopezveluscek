import PropTypes from "prop-types";

function InputForm({ name, onInputChange, title, value }) {
  return (
    <>
      <label style={{ width: "100px", marginRight: 4 }}>{title}</label>
      <input
        required={true}
        value={value}
        name={name}
        type="text"
        onChange={onInputChange}
      />
    </>
  );
}

InputForm.propTypes = {
  name: PropTypes.string,
  onInputChange: PropTypes.func,
  title: PropTypes.string,
  value: PropTypes.string
};

export default InputForm;
