import { useContext } from "react";
import { FaCartPlus } from "react-icons/fa";

import { Link } from "react-router-dom";

import cartContext from "storage/CartContext";

import "./CartWidget.css";

function CartWidget() {
  const { totalItemsInCart } = useContext(cartContext);

  return (
    <div className="chango">
      {totalItemsInCart() > 0 ? (
        <>
          <span>{totalItemsInCart()}</span>
        </>
      ) : (
        <></>
      )}
      <Link to="/cart">
        <FaCartPlus />
      </Link>
    </div>
  );
}

export default CartWidget;
