import PropTypes from "prop-types";

import "./flexwrapper.css";

function FlexWrapper({ children }) {
  return <div className="flex">{children}</div>;
}

FlexWrapper.propTypes = {
  children: PropTypes.node
};

export default FlexWrapper;
