import { useState } from "react";
import PropTypes from "prop-types";

import Button from "components/Button/Button";

import "./itemcount.css";

function ItemCount({ inCart, onAddToCart, stock, text }) {
  const [count, setCount] = useState(1);

  function handleAdd() {
    if (count < stock) {
      setCount((c) => c + 1);
    }
  }

  function handleSubstract() {
    if (count > 1) {
      setCount((c) => c - 1);
    }
  }

  return (
    <div className="itemcount_container">
      {inCart > 0 && <div>Hay {inCart} unidades ya en el carrito</div>}
      <div className="itemcount_control">
        <Button color="#903024" onClick={handleSubstract}>
          -
        </Button>
        <span>{count}</span>
        <Button color="#239044" onClick={handleAdd}>
          +
        </Button>
      </div>
      <div className="itemcount_btns">
        <Button type="alert" onClick={() => onAddToCart(count)}>
          {text}
        </Button>
      </div>
    </div>
  );
}

ItemCount.propTypes = {
  inCart: PropTypes.number,
  onAddToCart: PropTypes.func,
  stock: PropTypes.number,
  text: PropTypes.string
};

export default ItemCount;
