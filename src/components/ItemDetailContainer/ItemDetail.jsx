import { useContext } from "react";
import PropTypes from "prop-types";

import ItemCount from "components/ItemCount/ItemCount";
import cartContext from "storage/CartContext";

import "./itemdetail.css";

function ItemDetail({ product }) {
  const { cart, addToCart } = useContext(cartContext);

  let itemInCart = cart.find((item) => product.id === item.id);
  let stock = product.stock;
  let inCart = 0;
  if (itemInCart) {
    inCart = itemInCart.count;
    stock -= inCart;
  }

  function onAddToCart(count) {
    const itemForCart = {
      ...product,
      count
    };

    addToCart(itemForCart);
  }

  return (
    <div className="detail">
      <div className="card-detail_img">
        <img src={product.thumbnail} alt="Product img" />
      </div>
      <div className="card-detail_detail">
        <h2>{product.title}</h2>
        <p>{product.description}</p>
        <h4 className="priceTag">$ {product.price}</h4>
      </div>
      <div className="grid-cart">
        <ItemCount
          inCart={inCart}
          onAddToCart={onAddToCart}
          stock={stock}
          text="Agregar al carrito"
        />
      </div>
    </div>
  );
}

ItemDetail.propTypes = {
  product: PropTypes.object
};

export default ItemDetail;
