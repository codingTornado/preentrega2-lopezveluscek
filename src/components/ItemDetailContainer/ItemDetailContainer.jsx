import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";

import { getSingleItemFromAPI } from "services/firebase";
import ItemDetail from "./ItemDetail";

import Loader from "components/Loader/Loader";
import FlexWrapper from "components/FlexWrapper/FlexWrapper";

function ItemDetailContainer() {
  const [product, setProduct] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const [feedbackMsg, setFeedbackMsg] = useState(null);

  let id = useParams().id;

  useEffect(() => {
    getSingleItemFromAPI(id)
      .then((itemsDB) => {
        setProduct(itemsDB);
      })
      .catch((error) => {
        setFeedbackMsg(`Error: ${error.message}`);
      })
      .finally(() => setIsLoading(false));
  }, [id]);

  if (isLoading)
    return (
      <FlexWrapper>
        <Loader color="blue" size={128} />
      </FlexWrapper>
    );

  return (
    <div>
      {feedbackMsg ? (
        <span style={{ backgroundColor: "pink" }}>{feedbackMsg}</span>
      ) : (
        <ItemDetail product={product} />
      )}
    </div>
  );
}

export default ItemDetailContainer;
