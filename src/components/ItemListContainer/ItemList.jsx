import PropTypes from "prop-types";

import FlexWrapper from "components/FlexWrapper/FlexWrapper";
import Item from "components/Item/Item";
import Loader from "components/Loader/Loader";

function ItemList({ productsList, feedbackMsg }) {
  let emptyArray = productsList.length === 0;

  return (
    <FlexWrapper>
      {emptyArray ? (
        feedbackMsg ? (
          <span style={{ backgroundColor: "pink" }}>{feedbackMsg}</span>
        ) : (
          <Loader color="green" size={128} />
        )
      ) : (
        productsList.map((product) => (
          <Item key={product.id} product={product} />
        ))
      )}
    </FlexWrapper>
  );
}

ItemList.propTypes = {
  feedbackMsg: PropTypes.string,
  productsList: PropTypes.arrayOf(PropTypes.object)
};

export default ItemList;
