import { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import PropTypes from "prop-types";

import { getItemsFromAPI, getItemsFromAPIByCategory } from "services/firebase";

import FlexWrapper from "components/FlexWrapper/FlexWrapper";
import ItemList from "./ItemList";
import Loader from "components/Loader/Loader";

function ItemListContainer() {
  const [productsList, setProductsList] = useState([]);
  const [feedbackMsg, setFeedbackMsg] = useState(null);
  const [isLoading, setIsLoading] = useState(true);

  const { categoryid } = useParams();

  useEffect(() => {
    setIsLoading(true);
    if (categoryid) {
      getItemsFromAPIByCategory(categoryid)
        .then((itemsDB) => {
          setProductsList(itemsDB);
        })
        .catch((error) => {
          setFeedbackMsg(error.message);
        })
        .finally(() => setIsLoading(false));
    } else {
      getItemsFromAPI()
        .then((itemsDB) => {
          setProductsList(itemsDB);
        })
        .finally(() => setIsLoading(false));
    }
  }, [categoryid]);

  if (isLoading) {
    return (
      <FlexWrapper>
        <Loader color="blue" size={128} />
      </FlexWrapper>
    );
  }

  return <ItemList feedbackMsg={feedbackMsg} productsList={productsList} />;
}

ItemListContainer.propTypes = {
  children: PropTypes.node
};

export default ItemListContainer;
