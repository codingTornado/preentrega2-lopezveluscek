import { LeapFrog } from "@uiball/loaders";

function Loader(props) {
  return <LeapFrog {...props} />;
}

export default Loader;
