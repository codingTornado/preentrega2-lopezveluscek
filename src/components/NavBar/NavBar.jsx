import CartWidget from "components/CartWidget/CartWidget";
import { Link } from "react-router-dom";

import "./navbar.css";

export default function NavBar() {
  return (
    <>
      <nav>
        <div>
          <ul>
            <li>
              <Link to="/">
                <h4>i comers</h4>
              </Link>
            </li>

            <li>
              <Link to="/category/comics">Comics</Link>
            </li>

            <li>
              <Link to="/category/mangas">Manga</Link>
            </li>

            <li>
              <Link to="/category/boardgames">Juegos de mesa</Link>
            </li>
          </ul>
        </div>
        <CartWidget />
      </nav>
    </>
  );
}
