const itemsDB = [
  {
    id: 1,
    thumbnail:
      "https://i5.walmartimages.com/asr/153f4f9c-c170-47d3-9915-8f1084423516_1.14752f9fb1a9ff57b7a428190c4378d8.jpeg",
    title: "Comic Test 1",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam varius semper tortor, in finibus erat. Proin dictum tortor scelerisque metus faucibus, venenatis pretium ligula placerat. Quisque euismod fringilla massa, vel varius neque pharetra sed. Quisque malesuada, dui in interdum placerat, lorem ex imperdiet nulla, sed malesuada libero dui eget ipsum. Vestibulum sit amet erat id libero ultrices suscipit. Etiam nec ullamcorper tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean semper ante felis, id suscipit tellus gravida pellentesque. Donec luctus a arcu eget tristique. Nam molestie est vitae maximus suscipit.",
    price: 123,
    stock: 3,
    category: "comics"
  },
  {
    id: 2,
    thumbnail:
      "https://i5.walmartimages.com/asr/153f4f9c-c170-47d3-9915-8f1084423516_1.14752f9fb1a9ff57b7a428190c4378d8.jpeg",
    title: "Comic Test 2",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam varius semper tortor, in finibus erat. Proin dictum tortor scelerisque metus faucibus, venenatis pretium ligula placerat. Quisque euismod fringilla massa, vel varius neque pharetra sed. Quisque malesuada, dui in interdum placerat, lorem ex imperdiet nulla, sed malesuada libero dui eget ipsum. Vestibulum sit amet erat id libero ultrices suscipit. Etiam nec ullamcorper tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean semper ante felis, id suscipit tellus gravida pellentesque. Donec luctus a arcu eget tristique. Nam molestie est vitae maximus suscipit.",
    price: 123,
    stock: 7,
    category: "comics"
  },
  {
    id: 3,
    thumbnail:
      "https://i5.walmartimages.com/asr/153f4f9c-c170-47d3-9915-8f1084423516_1.14752f9fb1a9ff57b7a428190c4378d8.jpeg",
    title: "Comic Test 3",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam varius semper tortor, in finibus erat. Proin dictum tortor scelerisque metus faucibus, venenatis pretium ligula placerat. Quisque euismod fringilla massa, vel varius neque pharetra sed. Quisque malesuada, dui in interdum placerat, lorem ex imperdiet nulla, sed malesuada libero dui eget ipsum. Vestibulum sit amet erat id libero ultrices suscipit. Etiam nec ullamcorper tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean semper ante felis, id suscipit tellus gravida pellentesque. Donec luctus a arcu eget tristique. Nam molestie est vitae maximus suscipit.",
    price: 123,
    stock: 4,
    category: "comics"
  },
  {
    id: 4,
    thumbnail:
      "https://i5.walmartimages.com/asr/153f4f9c-c170-47d3-9915-8f1084423516_1.14752f9fb1a9ff57b7a428190c4378d8.jpeg",
    title: "Comic Test 4",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam varius semper tortor, in finibus erat. Proin dictum tortor scelerisque metus faucibus, venenatis pretium ligula placerat. Quisque euismod fringilla massa, vel varius neque pharetra sed. Quisque malesuada, dui in interdum placerat, lorem ex imperdiet nulla, sed malesuada libero dui eget ipsum. Vestibulum sit amet erat id libero ultrices suscipit. Etiam nec ullamcorper tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean semper ante felis, id suscipit tellus gravida pellentesque. Donec luctus a arcu eget tristique. Nam molestie est vitae maximus suscipit.",
    price: 123,
    stock: 5,
    category: "comics"
  },
  {
    id: 5,
    thumbnail:
      "https://1rwu1s8n7d53176bt1dq2up3-wpengine.netdna-ssl.com/wp-content/uploads/2020/05/Tsutomu-Niheis-BLAME-CyberPunks.com-Book-Cover-3-768x1087-1.jpg",
    title: "Manga Test 1",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam varius semper tortor, in finibus erat. Proin dictum tortor scelerisque metus faucibus, venenatis pretium ligula placerat. Quisque euismod fringilla massa, vel varius neque pharetra sed. Quisque malesuada, dui in interdum placerat, lorem ex imperdiet nulla, sed malesuada libero dui eget ipsum. Vestibulum sit amet erat id libero ultrices suscipit. Etiam nec ullamcorper tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean semper ante felis, id suscipit tellus gravida pellentesque. Donec luctus a arcu eget tristique. Nam molestie est vitae maximus suscipit.",
    price: 123,
    stock: 4,
    category: "mangas"
  },
  {
    id: 6,
    thumbnail:
      "https://1rwu1s8n7d53176bt1dq2up3-wpengine.netdna-ssl.com/wp-content/uploads/2020/05/Tsutomu-Niheis-BLAME-CyberPunks.com-Book-Cover-3-768x1087-1.jpg",
    title: "Manga Test 2",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam varius semper tortor, in finibus erat. Proin dictum tortor scelerisque metus faucibus, venenatis pretium ligula placerat. Quisque euismod fringilla massa, vel varius neque pharetra sed. Quisque malesuada, dui in interdum placerat, lorem ex imperdiet nulla, sed malesuada libero dui eget ipsum. Vestibulum sit amet erat id libero ultrices suscipit. Etiam nec ullamcorper tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean semper ante felis, id suscipit tellus gravida pellentesque. Donec luctus a arcu eget tristique. Nam molestie est vitae maximus suscipit.",
    price: 123,
    stock: 5,
    category: "mangas"
  },
  {
    id: 7,
    thumbnail:
      "https://1rwu1s8n7d53176bt1dq2up3-wpengine.netdna-ssl.com/wp-content/uploads/2020/05/Tsutomu-Niheis-BLAME-CyberPunks.com-Book-Cover-3-768x1087-1.jpg",
    title: "Manga Test 3",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam varius semper tortor, in finibus erat. Proin dictum tortor scelerisque metus faucibus, venenatis pretium ligula placerat. Quisque euismod fringilla massa, vel varius neque pharetra sed. Quisque malesuada, dui in interdum placerat, lorem ex imperdiet nulla, sed malesuada libero dui eget ipsum. Vestibulum sit amet erat id libero ultrices suscipit. Etiam nec ullamcorper tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean semper ante felis, id suscipit tellus gravida pellentesque. Donec luctus a arcu eget tristique. Nam molestie est vitae maximus suscipit.",
    price: 123,
    stock: 8,
    category: "mangas"
  },
  {
    id: 8,
    thumbnail:
      "https://1rwu1s8n7d53176bt1dq2up3-wpengine.netdna-ssl.com/wp-content/uploads/2020/05/Tsutomu-Niheis-BLAME-CyberPunks.com-Book-Cover-3-768x1087-1.jpg",
    title: "Manga Test 4",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam varius semper tortor, in finibus erat. Proin dictum tortor scelerisque metus faucibus, venenatis pretium ligula placerat. Quisque euismod fringilla massa, vel varius neque pharetra sed. Quisque malesuada, dui in interdum placerat, lorem ex imperdiet nulla, sed malesuada libero dui eget ipsum. Vestibulum sit amet erat id libero ultrices suscipit. Etiam nec ullamcorper tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean semper ante felis, id suscipit tellus gravida pellentesque. Donec luctus a arcu eget tristique. Nam molestie est vitae maximus suscipit.",
    price: 123,
    stock: 2,
    category: "mangas"
  },
  {
    id: 9,
    thumbnail:
      "https://www.ehgaming.com/wp-content/uploads/2019/12/root-board-game.jpg",
    title: "BoardGame Test 1",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam varius semper tortor, in finibus erat. Proin dictum tortor scelerisque metus faucibus, venenatis pretium ligula placerat. Quisque euismod fringilla massa, vel varius neque pharetra sed. Quisque malesuada, dui in interdum placerat, lorem ex imperdiet nulla, sed malesuada libero dui eget ipsum. Vestibulum sit amet erat id libero ultrices suscipit. Etiam nec ullamcorper tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean semper ante felis, id suscipit tellus gravida pellentesque. Donec luctus a arcu eget tristique. Nam molestie est vitae maximus suscipit.",
    price: 123,
    stock: 5,
    category: "boardgames"
  },
  {
    id: 10,
    thumbnail:
      "https://www.ehgaming.com/wp-content/uploads/2019/12/root-board-game.jpg",
    title: "BoardGame Test 2",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam varius semper tortor, in finibus erat. Proin dictum tortor scelerisque metus faucibus, venenatis pretium ligula placerat. Quisque euismod fringilla massa, vel varius neque pharetra sed. Quisque malesuada, dui in interdum placerat, lorem ex imperdiet nulla, sed malesuada libero dui eget ipsum. Vestibulum sit amet erat id libero ultrices suscipit. Etiam nec ullamcorper tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean semper ante felis, id suscipit tellus gravida pellentesque. Donec luctus a arcu eget tristique. Nam molestie est vitae maximus suscipit.",
    price: 123,
    stock: 4,
    category: "boardgames"
  },
  {
    id: 11,
    thumbnail:
      "https://www.ehgaming.com/wp-content/uploads/2019/12/root-board-game.jpg",
    title: "BoardGame Test 3",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam varius semper tortor, in finibus erat. Proin dictum tortor scelerisque metus faucibus, venenatis pretium ligula placerat. Quisque euismod fringilla massa, vel varius neque pharetra sed. Quisque malesuada, dui in interdum placerat, lorem ex imperdiet nulla, sed malesuada libero dui eget ipsum. Vestibulum sit amet erat id libero ultrices suscipit. Etiam nec ullamcorper tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean semper ante felis, id suscipit tellus gravida pellentesque. Donec luctus a arcu eget tristique. Nam molestie est vitae maximus suscipit.",
    price: 123,
    stock: 4,
    category: "boardgames"
  },
  {
    id: 12,
    thumbnail:
      "https://www.ehgaming.com/wp-content/uploads/2019/12/root-board-game.jpg",
    title: "BoardGame Test 4",
    description:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam varius semper tortor, in finibus erat. Proin dictum tortor scelerisque metus faucibus, venenatis pretium ligula placerat. Quisque euismod fringilla massa, vel varius neque pharetra sed. Quisque malesuada, dui in interdum placerat, lorem ex imperdiet nulla, sed malesuada libero dui eget ipsum. Vestibulum sit amet erat id libero ultrices suscipit. Etiam nec ullamcorper tellus. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Aenean semper ante felis, id suscipit tellus gravida pellentesque. Donec luctus a arcu eget tristique. Nam molestie est vitae maximus suscipit.",
    price: 123,
    stock: 6,
    category: "boardgames"
  }
];

export default itemsDB;
