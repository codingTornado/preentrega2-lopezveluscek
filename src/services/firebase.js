import { initializeApp } from "firebase/app";
import {
  getFirestore,
  collection,
  getDocs,
  doc,
  getDoc,
  query,
  where,
  addDoc,
  writeBatch,
  documentId,
  getCountFromServer
} from "firebase/firestore";

import items from "data/products";

const firebaseConfig = {
  apiKey: "AIzaSyDOcuMjfUWFP7asfxS_YDDDnpyeGV5fO-k",
  authDomain: "react34785-9a72f.firebaseapp.com",
  projectId: "react34785-9a72f",
  storageBucket: "react34785-9a72f.appspot.com",
  messagingSenderId: "669118112177",
  appId: "1:669118112177:web:508d91f9f283145a815d90"
};

const FirebaseApp = initializeApp(firebaseConfig);

const DB = getFirestore(FirebaseApp);

export function testDatabase() {
  console.log(FirebaseApp);
}

export async function getSingleItemFromAPI(id) {
  const docRef = doc(DB, "products", id);
  const docSnap = await getDoc(docRef);

  if (docSnap.exists()) {
    return {
      ...docSnap.data(),
      id: docSnap.id
    };
  } else {
    throw new Error("El producto no existe");
  }
}

export async function getItemsFromAPI() {
  try {
    const collectionProducts = collection(DB, "products");
    const is_init = await getCountFromServer(collectionProducts);
    if (is_init.data().count === 0) {
      console.log("Initializing database");
      exportItemsToFirestore();
    }

    let respuesta = await getDocs(collectionProducts);

    const products = respuesta.docs.map((docu) => {
      return {
        ...docu.data(),
        id: docu.id
      };
    });

    return products;
  } catch (error) {
    console.error(error);
  }
}

export async function getItemsFromAPIByCategory(categoryId) {
  const productsRef = collection(DB, "products");
  const myQuery = query(productsRef, where("category", "==", categoryId));

  const productsSnap = await getDocs(myQuery);

  const emptyArray = productsSnap.docs.length < 1;

  if (emptyArray) throw new Error("Categoría sin resultados");

  const products = productsSnap.docs.map((docu) => {
    return {
      ...docu.data(),
      id: docu.id
    };
  });

  return products;
}

export async function createBuyOrderFirestore(buyOrderData) {
  const collectionRef = collection(DB, "buyorders");
  const docRef = await addDoc(collectionRef, buyOrderData);

  return docRef.id;
}

export async function createBuyOrderFirestoreWithStock(buyOrderData) {
  const collectionProductsRef = collection(DB, "products");
  const collectionOrdersRef = collection(DB, "buyorders");
  const batch = writeBatch(DB);

  let arrayIds = buyOrderData.items.map((item) => {
    return item.id;
  });

  const q = query(collectionProductsRef, where(documentId(), "in", arrayIds));

  let productsSnapshot = await getDocs(q);

  productsSnapshot.docs.forEach((doc) => {
    let stockActual = doc.data().stock;
    let itemInCart = buyOrderData.items.find((item) => item.id === doc.id);
    let stockActualizado = stockActual - itemInCart.count;

    batch.update(doc.ref, { stock: stockActualizado });
  });

  const docOrderRef = doc(collectionOrdersRef);
  batch.set(docOrderRef, buyOrderData);

  batch.commit();

  return docOrderRef.id;
}

async function exportItemsToFirestore() {
  const collectionRef = collection(DB, "products");

  for (let item of items) {
    item.index = item.id;
    delete item.id;
    const docRef = await addDoc(collectionRef, item);
    console.log("Document created with ID", docRef.id);
  }
}
